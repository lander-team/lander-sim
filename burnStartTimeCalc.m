function [h0, vb, burnStartTime] = burnStartTimeCalc(Tcurve, tb, M0, mdot, Mb, v0)
g = -9.81;
v_prev = v0;
h_prev = 0;

for i = 1:length(Tcurve)
    if i < length(Tcurve)
        dt = Tcurve(i + 1, 1) - Tcurve(i, 1);
        a = (Tcurve(i, 2) / (M0 - mdot * Tcurve(i + 1, 1))) + g;
    else
        dt = 0;
        a = (Tcurve(i, 2) / Mb) + g;
    end
    
    v = a * dt + v_prev;
    v_prev = v;
    h = v*dt + h_prev;
    h_prev = h;
end

hb = h;
hf = v^2 / (2*-g);
h0 = hb + hf;
vb = v;
burnStartTime = vb / -g;