# Lander Landing Simulation

## Results

![Position Over Time Animation](outputs/Altitude.gif)

![Accel-Vel-Alt vs Time](outputs/Accel-Vel-Alt_vs_Time.png)

![Euler Angles vs Time](outputs/Euler_Angles_vs_Time.png)

![Servo Position vs Time](outputs/Servo_Position_vs_Time.png)

## Requirements

- MATLAB r2021a
- Aerospace Toolbox
- Aerospace Blockset
- DSP System Toolbox
